# Galaxy Mobile Japan CMS | Java, Spring5, MySQL

## AP 정보

* Java : OpenJDK `9+181`
* OS: AWS EC2 Amazon AMI2
* Web : nginx `1.13.12`
* Was : tomcat `9.0.6`
* DB : AWS RDS Aurora MySQL `5.6.10a`
* File System : AWS S3

## OTP LOGIN
#### security-context.xml
```
<beans:bean id="customAuthenticationProvider" class="jp.galaxymobile.admin.login.security.otp.CustomAuthenticationProvider">
		<beans:property name="userDetailsService" ref="gmjUserService"/>
		<beans:property name="passwordEncoder" ref="encoder"/>
	</beans:bean>
```

#### CustomAuthenticationProvider.java
```
package jp.galaxymobile.admin.login.security.otp;

import jp.galaxymobile.admin.login.model.AdminUserVO;
import jp.galaxymobile.admin.login.security.GMJUserService;
import org.jboss.aerogear.security.otp.Totp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider extends DaoAuthenticationProvider {

	@Autowired
	GMJUserService gmjUserService;

	@Override
	public Authentication authenticate(Authentication auth)
			throws AuthenticationException {
		String user = auth.getName();
		String verificationCode
				= ((CustomWebAuthenticationDetails) auth.getDetails())
				.getVerificationCode();

		if ((user == null)) {
			throw new BadCredentialsException("Invalid username or password");
		}

		AdminUserVO adminUserVO = (AdminUserVO) gmjUserService.loadUserByUsername(user);

		if (adminUserVO.getOprUserStatus() == 1) throw new BadCredentialsException("the account is locked");

		Totp totp = new Totp(adminUserVO.getUserOtpKey());

		if (!isValidLong(verificationCode) || !totp.verify(verificationCode)) {
			throw new BadCredentialsException("Invalid verfication code");
		}

		Authentication result = super.authenticate(auth);

		return new UsernamePasswordAuthenticationToken(
				result.getPrincipal(), result.getCredentials(), result.getAuthorities());
	}
}
```

## Sites/Pages Landing
### AdminSitesPagesController.java
```
package jp.galaxymobile.admin.pages.controller;

import ...

/**
 * Admin Sites Pages Phase2 관련 컨트롤러
 * @author 1010
 *
 */
@Controller
@RequestMapping("/gmja/sites")
public class AdminSitesPagesController {
	
	/**
	 * log 객체
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AdminSitesPagesController.class);
	
	/**
	 * AdminSitesPagesService
	 */
	@Autowired
    AdminSitesPagesService adminSitesPagesService;
    /**
     * AdminAssetsService
     */
    @Autowired
    AdminAssetsService adminAssetsService;

	/**
	 *
	 * <pre>
	 * 1. 개요 : SITES > PAGES 게시판 목록조회
	 * 2. 처리 : SITES > PAGES 게시판 목록조회 공통사용
	 * </pre>
	 *
	 * @Method Name : pageList
	 *
	 * @param request
	 * @param adminSitesPagesVO
	 * @return
	 */
	@RequestMapping("/pages/list")
	public ModelAndView pageList(HttpServletRequest request, AdminSitesPagesVO adminSitesPagesVO) {

		LOG.debug("/pages/list 호출");

		ModelAndView mav = new ModelAndView("sites/pages/list");

		if(request.getParameter("pagePath") != null && !"".equals(request.getParameter("pagePath"))) {
			mav.addObject("pagePath", adminSitesPagesVO.getPagePath());
		}
        return mav;
	}
	
	...
}
```

### WEB-INF/views/sites/pages/list.jsp
<img src="./99.imgs/img2.png">


### AdminApiController.java
```
package jp.galaxymobile.admin.common.controller;

import ...

/**
 * <pre>
 * jp.galaxymobile.admin.common.controller
 * AdminApiController.java
 *
 * 어드민 API 관련 컨트롤러 .
 * ajax로 호출해서 결과값을 json 형태로 제공하는 request는 이 컨트롤러에 추가한다.
 * service, dao, model은 해당 비지니스 패키지에 생성한다.
 * 여기에서 생성되는 메소드의 url은 /api로 시작되게 한다.
 * </pre>
 *
 * @Auther : 1010
 */
@Slf4j
@RestController
@RequestMapping("/gmja")
public class AdminApiController {
    
    /**
     * AdminSitesPagesService
     */
    @Autowired
    AdminSitesPagesService adminSitesPagesService;
    
    ...
    
    /**
     * AdminAssetsService
     */
    @Autowired
    AdminAssetsService adminAssetsService;

    /**
     *
     * <pre>
     * 1. 개요 : SITES > PAGES 게시판 관련 컨텐츠 목록 조회
     * 2. 처리 : SITES > PAGES 게시판 관련 컨텐츠 목록 조회
     * </pre>
     *
     * @Method Name : getAuthPageRootData
     *
     * @param request	HttpServletRequest
     * @param response	HttpServletResponse
     * @return	관련 컨텐츠 목록 json
     */
    @RequestMapping(value={"/api/sitesPages/getAuthPageRootData"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<AdminSitesPagesVO> getAuthPageRootData(HttpServletRequest request, HttpServletResponse response) throws IOException {

        log.debug("/api/sitesPages/getAuthPageRootData 호출");

        AdminUserVO adminUserVO = (AdminUserVO) request.getSession(false).getAttribute("userSessionInfoVO");

        // 페이지 절대 권한 배열
        // SUPERUSER_ROLE, PENGTAI_ROLE, CHEIL_HQ_ROLE, SAMSUNG_JAPAN_ROLE, SAMSUNG_HQ_ROLE
        ArrayList<String> authLevelArr = new ArrayList<>(Arrays.asList(CommonUtil.getProp("cms.auth.superusers").split(",")));

        return authLevelArr.contains(Integer.toString(adminUserVO.getUserLevel())) ? adminSitesPagesService.getPageRootData():adminSitesPagesService.getAuthPageRootData(adminUserVO);
    }
}

```

### AdminSitesPagesService.java
```
package jp.galaxymobile.admin.pages.service;

import ...

/**
 * SITES > PAGES 관련 처리.
 *
 * @author 1010
 *
 */
@Service
public class AdminSitesPagesService {
	
	/**
	 * 로그 객체
	 */
	private static final Logger LOG = LoggerFactory.getLogger(AdminSitesPagesService.class);
	
    /**
	 * dao단 매퍼 인젝션. 나중에 userMapper 로 변경
	 */
	@Inject
    AdminSitesPagesMapper adminSitesPagesMapper;

	@Inject
	AdminUserListMapper adminUsertListMapper;

	@Autowired
	AdminUserListService adminUserListService;

	@Autowired
	Aes256Service aes256Service;

	@Autowired
	AwsService awsService;

	@Inject
	ReloadableFilterInvocationSecurityMetadataSource reloadableFilterInvocationSecurityMetadataSource;

	/**
	 *	was 서버에 업로드할 서브 디렉토리 
	 */
	@Value("#{configProps['upload.subDir.pages']}")
	private String subDir;

	@Value("${spring.profiles.active:Unknown}")
	private String activeProfile;

	/**
	 *	페이지 절대 권한 배열 (SUPERUSER_ROLE, PENGTAI_ROLE, CHEIL_HQ_ROLE, SAMSUNG_JAPAN_ROLE, SAMSUNG_HQ_ROLE)
	 */
	@Value("#{configProps['cms.auth.superusers']}")
	private String[] authLevelArr;

	/**
	 * 글자수 제한때 사용
	 */
	private static final int MAX_LENGTH = 4000;

	/**
	 *
	 * <pre>
	 * 1. 개요 : SITES > PAGES ROOT LIST 조회
	 * 2. 처리 : SITES > PAGES ROOT LIST 조회
	 * </pre>
	 *
	 * @Method Name : getAuthPageRootData
	 * @param adminUserVO AdminUserVO
	 * @return list List<AdminSitesPagesVO>
	 */
	public List<AdminSitesPagesVO> getAuthPageRootData(AdminUserVO adminUserVO) {
		LOG.debug("Authorized Sites Pages Root LIST 조회 시작");

		String sitesPrefix = CommonUtil.getProp("s3.prefix") + "/resources/sites/";
		String[] illegalFiles = CommonUtil.getProp("cms.pages.illegalFiles").split(",");

		ObjectListing resList = CommonFileUtil.getS3FileList(sitesPrefix, activeProfile);

		List<AdminPageAuthVO> roleListRootPath = getRoleRootPathForPage(adminUserVO, "ROTY0102", true, false);

		List<AdminSitesPagesVO> list = new ArrayList<AdminSitesPagesVO>();
		List<S3ObjectSummary> sumList = resList.getObjectSummaries();

		for (final S3ObjectSummary objectSummary: sumList) {

			String key = objectSummary.getKey();
			String shortenKey = key.replace(sitesPrefix,"");

			String[] keyArr = shortenKey.split("\\/");

			if(shortenKey == null || shortenKey.isEmpty() || keyArr.length > 1) continue;
			if(Arrays.asList(illegalFiles).contains(keyArr[keyArr.length-1])) continue;

			for(AdminPageAuthVO arp2 : roleListRootPath){
				if(shortenKey.startsWith(arp2.getAllowRegion())){
					if(list.stream().filter(a -> a.getPagePath() == shortenKey).count() > 0) continue;
					AdminSitesPagesVO adminSitesPagesVO = new AdminSitesPagesVO();

					adminSitesPagesVO.setPagePath(shortenKey);
					adminSitesPagesVO.setFileName(keyArr[keyArr.length-1]);
					adminSitesPagesVO.setType("D");

					Boolean hasChild = false;

					List<S3ObjectSummary> contentSum = sumList.stream().filter(
							a -> a.getKey().startsWith(key) && a.getKey().replace(key,"").split("\\/").length == 1).collect(Collectors.toList());

					if(contentSum.size() > 1){
						for(final S3ObjectSummary childFileSummary: contentSum){
							String childFileKey = childFileSummary.getKey().replace(key,"");
							String[] nextKeyArr = childFileKey.split("\\/");

							if(!Arrays.asList(illegalFiles).contains(nextKeyArr[nextKeyArr.length-1])) hasChild = true;

							if(nextKeyArr[nextKeyArr.length-1].equals(".content.xml")){
								adminSitesPagesVO.setType("F");

								AdminSitesPagesXml xml = new AdminSitesPagesXml(adminSitesPagesVO, childFileSummary);
								adminSitesPagesVO = xml.convertToPage();

							}else{
								adminSitesPagesVO.setLastUdtDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(childFileSummary.getLastModified()));
							}
						}

					}

					adminSitesPagesVO.setHasChild(hasChild);

					list.add(adminSitesPagesVO);
				}
			}

		}

		return list;
	}

}
```

### CommonFileUtil.java

```
package jp.galaxymobile.admin.common.util;

import ...


/**
 * 공통 파일, 파일 업로드 관련  유틸리티 클래스
 *
 * @author 1010
 *
 */
public class CommonFileUtil {

	/**
	 * 로그 객체
	 */
	private final static Logger LOG = LoggerFactory.getLogger(CommonFileUtil.class);

	public static ObjectListing getS3FileList(String prefix, String activeProfile) {
		ObjectListing resList = new ObjectListing();

		try {
			//AWS
			AmazonS3 s3Client = buildS3Client(activeProfile);

			String assetsPrefix = prefix;

			final ListObjectsRequest listObjectRequest = new ListObjectsRequest().
					withBucketName(CommonUtil.getProp("s3.bucket")).
					withPrefix(assetsPrefix);

			resList = s3Client.listObjects(listObjectRequest);

		} catch(IllegalStateException e) {
			LOG.error("transferFile Error => " + e.getMessage(), e);
		}

		return resList;
	}

	public static AmazonS3 buildS3Client(String activeProfile) {
        AmazonS3 s3Client;

		if(activeProfile != null && "local".equalsIgnoreCase(activeProfile)){
            ClientConfiguration config = new ClientConfiguration();
            config.setProtocol(Protocol.HTTP);
			config.setProxyHost("121.252.4.178");
			config.setProxyPort(8080);

            BasicAWSCredentials awsCreds = new BasicAWSCredentials(CommonUtil.getProp("s3.accesskey"), CommonUtil.getProp("s3.secretkey"));
            s3Client = AmazonS3ClientBuilder.standard()
                    .withClientConfiguration(config)
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                    .withRegion("ap-northeast-2")
                    .build();
		}else{
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(CommonUtil.getProp("s3.accesskey"), CommonUtil.getProp("s3.secretkey"));
            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                    .withRegion("ap-northeast-2")
                    .build();
        }

		return s3Client;
	}
}
```

### Amazon S3 /해당 page 경로/.content.xml

```
<?xml version="1.0" encoding="UTF-8"?>
<root xmlns:gmja="http://www.jcp.org/jcr/1.0"
        gmja:resourceType="page"
      gmja:name="test2"
      gmja:title="test2"
      gmja:lastUdtDate="2019-02-18 18:31:15"
      gmja:authLevel="2"
      gmja:metaTitle="test2"
      gmja:metaDesc="test2"
      gmja:metaKeywords="test2"
      gmja:thumbUrl="/resources/mdiphase2/test/testsetsetst/test2/v.1.0/1545890034692.jpg"
      gmja:thumbAlt="test2"
      gmja:author="test.partner@gmail.com"
      gmja:holder="test.partner@gmail.com">
  <header gmja:destElement="header#header">
    <components>
      <component gmja:resourceType="component"
                 gmja:path="/common/gmj/header" />
    </components>
  </header>
  <content gmja:destElement="div[role=main]">
    <components>
      <component gmja:resourceType="component"
                 gmja:path="/sites/test/testsetsetst/test2" />
    </components>
  </content>
  <footer gmja:destElement="footer#footer">
    <components>
      <component gmja:resourceType="component"
                 gmja:path="/common/gmj/footer" />
    </components>
  </footer>
</root>
```

### Amazon S3 /해당 page 경로/.content.xml
<img src="./99.imgs/img3.png">

