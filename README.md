# Back-end Developer / Machine Learning Engineer Portfolio 

> 지원자: 손진광
> 
> 현재직 회사/부서: Pengtai Korea / Marketing Technical Group 4 Team
> 
> last update: 2019.04.19


이 문서는 실무 개발에서 개인이 작성한 코드들을 위주로 개발 역량 증빙을 위한 포트폴리오 목적의 코드 리뷰 문서를 안내한다.

> 중요! 절대 본 문서의 참조 및 수정할 수 없으며 현 상태를 유지하고 업데이트하지 않는다.

<img src="./99.imgs/img6.png">
<img src="./99.imgs/img7.png">
<img src="./99.imgs/img8.png">

## 목차

#### * [연구논문](./01.papers.md)

#### * [(2019) 삼성전자 한국총괄 온라인 미디어 랩 솔루션(개발중) ](./04.Media_solution.md)

#### * [(2018) Galaxy Mobile Japan CMS | Java, Spring5, MySQL ](./03.Java_GMJ_Admin.md)

#### * [(2018) Samsung.com Component(Image) Detection ML | Python](./02.Python_ML_server.md)

